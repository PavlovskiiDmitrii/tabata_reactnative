import React, { useState } from "react";
import { StyleSheet, View, Text } from "react-native";
import Timer from "./src/components/Timer/Timer";
import Button from "./src/components/Button/Button";
import PopUp from "./src/components/PopUp/PopUp";

import { LinearGradient } from "expo-linear-gradient";

export default function App() {
  const workGradient = ["rgb(236, 158, 5)", "rgb(255, 78, 0)"];
  const restGradient = ["rgb(126, 232, 250)", "rgb(128, 255, 114)"];
  const [gradient, setGradient] = useState(workGradient);
  const [level, setLevel] = useState(null);
  const [popup, setPopup] = useState(false);

  return (
    <View style={styles.container}>
      <LinearGradient
        colors={gradient}
        style={styles.linearGradient}
        start={{ x: 0.9, y: 0.3 }}
      />
      {!level && (
        <View>
          <Button
            title={"levle 1"}
            styles={stylesBtn}
            func={() => {
              setLevel({
                timeWork: 10,
                timeRest: 3,
                maxRoundCount: 10,
              });
            }}
          />
          <Button
            title={"levle 2"}
            styles={stylesBtn}
            func={() => {
              setLevel({
                timeWork: 30,
                timeRest: 20,
                maxRoundCount: 5,
              });
            }}
          />
          <Button
            title={"Custom levle"}
            styles={stylesBtn}
            func={() => {
              setPopup(true);
            }}
          />
        </View>
      )}
      {level && (
        <Timer
          timeWork={level.timeWork}
          timeRest={level.timeRest}
          maxRoundCount={level.maxRoundCount}
          setGradient={setGradient}
          workGradient={workGradient}
          restGradient={restGradient}
          setLevel={setLevel}
        />
      )}
      {popup && <PopUp setLevel={setLevel} setPopup={setPopup} children={<Text>POPUP</Text>} />}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  linearGradient: {
    height: "100%",
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
  },
});

const stylesBtn = StyleSheet.create({
  btn: {
    height: 50,
    width: 200,
    borderRadius: 6,
    backgroundColor: "#ffffff70",
    justifyContent: "center",
    alignItems: "center",
    borderColor: "#ffffff80",
    borderTopColor: "#ffffff33",
    borderTopWidth: 1,
    borderLeftColor: "#ffffff33",
    borderLeftWidth: 2,
    borderBottomColor: "#ffffff99",
    margin: 10,
  },
  btnText: {},
});
