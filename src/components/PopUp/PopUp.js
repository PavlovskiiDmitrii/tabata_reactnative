import React, { useState } from "react";
import { StyleSheet, View, TextInput } from "react-native";
import Button from "../Button/Button";

export default function PopUp(props) {
  const { children, setPopup, setLevel } = props;
  const [value1, onChangeText1] = useState('30');
  const [value2, onChangeText2] = useState('20');
  const [value3, onChangeText3] = useState('5');

  return (
    <View style={styles.container}>
      <TextInput
        style={textInput.const}
        onChangeText={(text) => onChangeText1(text)}
        keyboardType = 'number-pad'
        value={value1}
      />
      <TextInput
        style={textInput.const}
        onChangeText={(text) => onChangeText2(text)}
        keyboardType = 'number-pad'
        value={value2}
      />
      <TextInput
        style={textInput.const}
        onChangeText={(text) => onChangeText3(text)}
        keyboardType = 'number-pad'
        value={value3}
      />
      <Button
        title={"close"}
        styles={stylesBtnEsc}
        func={() => {
          setPopup(false);
        }}
      />
      <Button
        title={"GO"}
        styles={stylesBtnGo}
        func={() => {
          setLevel({
            timeWork: value1,
            timeRest: value2,
            maxRoundCount: value3,
          });
          setPopup(false);
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    left: 50,
    right: 50,
    top: 100,
    height: "80%",
    backgroundColor: "#ffffffcc",
    padding: 80,
    borderRadius: 6,
  },
  btnEsc: {

  }
});

const stylesBtnEsc = StyleSheet.create({
  btn: {
    height: 50,
    width: 50,
    borderRadius: 6,
    backgroundColor: "#ec9e05",
    justifyContent: "center",
    alignItems: "center",
    borderColor: "#ffffff80",
    borderTopWidth: 1,
    position: "absolute",
    right: 10,
    top: 10,
  },
  btnText: {},
});

const stylesBtnGo = StyleSheet.create({
  btn: {
    height: 50,
    borderRadius: 6,
    backgroundColor: "#ec9e05",
    justifyContent: "center",
    alignItems: "center",
    borderColor: "#ffffff80",
    borderTopWidth: 1,
    position: "absolute",
    right: 10,
    left: 10,
    bottom: 10,
  },
  btnText: {},
});

const textInput = StyleSheet.create({
  const: {
    height: 40,
    backgroundColor: "#ec9e0573",
    borderWidth: 2,
    borderColor: '#ec9e05',
    margin: 3,
    paddingHorizontal: '5%',
    borderRadius: 3,
    color: '#272727',
    fontSize: 20,
  }
})