import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import useInterval from "../../Utils/useInterval";
import Button from "../../components/Button/Button";
import { Audio } from "expo-av";
import myAudio from "../../../src/static/sounds/pik.mp3";

export default function Timer(props) {
  const {
    timeWork,
    timeRest,
    maxRoundCount,
    setGradient,
    workGradient,
    restGradient,
    setLevel
  } = props;
  const delay = 100;
  const [status, setStatus] = useState("work");
  const [raundCount, setRaundCount] = useState(1);
  const [sec, setSec] = useState(timeWork);
  const [miliSec, setMiliSec] = useState(0);
  const [isRunning, setIsRunning] = useState(false);

  async function playSound() {
    const { sound } = await Audio.Sound.createAsync(myAudio);
    await sound.playAsync();
  }

  useInterval(
    () => {
      setMiliSec(miliSec - 1);
    },
    delay,
    isRunning
  );
  useEffect(() => {
    if (miliSec === 0) {
      if (isRunning) {
        if (sec === 0) {
          status === "work" ? startRest() : startWork();
        } else {
          decSec(sec);
          sec <= 5 && playSound();
        }
      }
      setMiliSec(10);
    }
  }, [miliSec]);
  useEffect(() => {
    if (status === "rest") {
      setGradient(restGradient);
    }
    if (status === "work") {
      setGradient(workGradient);
    }
  }, [status]);
  useEffect(() => {
    if (isRunning) {
      if (sec === timeRest || sec === timeWork) {
        decSec();
      }
    }
  }, [sec]);
  const decSec = () => {
    setSec(sec - 1);
  };
  const timetStop = () => {
    timetPause();
    setMiliSec(0);
    setSec(timeWork);
    setStatus("work");
    console.log("STOP");
  };
  const timetPause = () => {
    setIsRunning(false);
    console.log("PAUSE");
  };
  const timetStart = () => {
    sec === timeWork && decSec();
    setIsRunning(true);
    console.log("START");
  };
  const startRest = () => {
    setSec(timeRest);
    setMiliSec(0);
    setStatus("rest");
  };
  const startWork = () => {
    setSec(timeWork);
    setMiliSec(0);
    setStatus("work");
    setRaundCount(raundCount + 1);
  };

  return (
    <View style={styles.container}>
      <Text style={styles.round}>
        Round - {raundCount}/{maxRoundCount}
      </Text>
      <Text style={styles.status}>{status === "work" ? "WORKKK" : "RESR"}</Text>
      <Text style={styles.count}>
        {sec}:{miliSec !== 10 ? miliSec : 0}
      </Text>
      <View style={styles.btnwrapp}>
        <Button
          title={"GO"}
          styles={stylesBtn}
          func={() => {
            timetStart();
          }}
        />
        <Button
          title={"PAUSE"}
          styles={stylesBtn}
          func={() => {
            timetPause();
          }}
        />
        <Button
          title={"STOP"}
          styles={stylesBtn}
          func={() => {
            timetStop();
          }}
        />
         <Button
          title={"OUT"}
          styles={stylesBtn}
          func={() => {
            setLevel(null);
          }}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
  },
  count: {
    fontSize: 80,
  },
  btnwrapp: {
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
    width: "100%",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    paddingVertical: 8,
    paddingHorizontal: 8,
    marginBottom: 16,
  },
  status: {
    fontSize: 30,
  },
  round: {
    fontSize: 20,
  },
});

const stylesBtn = StyleSheet.create({
  btn: {
    height: 40,
    width: 200,
    borderRadius: 6,
    backgroundColor: "#ffffff70",
    justifyContent: "center",
    alignItems: "center",
    borderColor: "#ffffff80",
    borderTopColor: '#ffffff33',
    borderTopWidth: 1,
    borderLeftColor: '#ffffff33',
    borderLeftWidth: 2,
    borderBottomColor: '#ffffff99',
    margin: 5,
  },
  btnText: {
  },
});
