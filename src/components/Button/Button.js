import React from "react";
import { TouchableOpacity, Text } from "react-native";

export default function App(props) {
  const { func, title, styles } = props;
  return (
    <TouchableOpacity
      style={styles.btn}
      onPress={() => {
        func();
      }}
    >
      <Text 
      style={styles.btnText}
      >{title}</Text>
    </TouchableOpacity>
  );
}

// const localStyles = StyleSheet.create({
//   btn: {
//     width: 200,
//     height: 50,
//     flex: 1,
//     justifyContent: "center",
//     alignItems: "center",
//     borderWidth: 1,
//     borderColor: "#ffffff80",
//     borderRadius: 6,
//     marginTop: 16,
//     backgroundColor: "#ffffff70",
//     borderTopColor: '#ffffff33',
//     borderTopWidth: 1,
//     borderLeftColor: '#ffffff33',
//     borderLeftWidth: 2,
//     borderBottomColor: '#ffffff99',
//   },
//   btnText: {
//     fontSize: 25,
//     fontWeight: 'bold'
//   }
// });
